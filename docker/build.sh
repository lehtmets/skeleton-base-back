#!/bin/bash

VERSION=${1:-latest}

docker build \
  --tag skeleton:$VERSION \
  --file docker/Dockerfile \
  --build-arg VERSION=$VERSION \
  .
