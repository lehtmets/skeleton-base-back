#!/bin/bash

JAVA_CMD="java $JAVA_OPTS -jar boot.jar $@"

echo "Executing: $JAVA_CMD"

exec $JAVA_CMD
