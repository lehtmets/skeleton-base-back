#!/bin/bash

export JAVA_OPTS="-server -Xmx128m"

docker stop skeleton
docker rm skeleton
docker run \
  --init \
  --env JAVA_OPTS \
  --name skeleton \
  --publish 8080:8080 \
  skeleton --spring.profiles.active=${1:-live}

