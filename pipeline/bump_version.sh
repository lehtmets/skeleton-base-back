#!/bin/bash

set -eu

OLD_VERSION=$(< version)

OLD_SEQ_PART=${OLD_VERSION##*.}
OLD_CAL_PART=${OLD_VERSION%.*}

NEW_CAL_PART=$(date +%y.%m)

if [ "$OLD_CAL_PART" = "$NEW_CAL_PART" ]; then
  NEW_SEQ_PART=$(( OLD_SEQ_PART + 1 ))
else
  NEW_SEQ_PART=0
fi

NEW_VERSION=$NEW_CAL_PART.$NEW_SEQ_PART

echo $NEW_VERSION > version
