import io.spring.gradle.dependencymanagement.dsl.DependencyManagementExtension
import org.springframework.boot.gradle.plugin.SpringBootPlugin

plugins {
    id("org.springframework.boot") version "2.2.4.RELEASE" apply false
}

subprojects {
    apply(plugin = "io.spring.dependency-management")

    repositories {
        mavenCentral()
    }

    configure<DependencyManagementExtension> {
        imports {
            mavenBom(SpringBootPlugin.BOM_COORDINATES)
        }
    }
}

project(":app") {
    subprojects {
        apply(plugin = "java-library")

        configure<JavaPluginConvention> {
            sourceCompatibility = JavaVersion.VERSION_11
        }

        tasks.withType<Test> {
            useJUnitPlatform()
        }
    }
}
