import java.io.File

rootProject.name = "skeleton"

File("app").list()?.forEach {
    include("app:$it")
}
