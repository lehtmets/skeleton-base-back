FROM gradle:jdk11 as builder

WORKDIR /work

ARG VERSION

COPY .gradle .gradle
COPY app app
COPY *.gradle.kts gradle.properties ./

ENV GRADLE_USER_HOME=/work/.gradle
RUN gradle -Pversion=$VERSION assemble

FROM openjdk:11-jre-slim as runner

WORKDIR /work

COPY docker/entrypoint.sh .
COPY --from=builder /work/app/boot/build/libs/boot.jar .

EXPOSE 8080
ENTRYPOINT ["bash", "entrypoint.sh"]
